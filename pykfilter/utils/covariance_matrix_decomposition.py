class CovarianceMatrixDecomposition(object):
    """
    Utility object for getting entries of a matrix of the form
    C = L * U^-1 * L^T.
    """

    def __init__(self, L, Ui):
        self.LUi = L * Ui
        self.L = L
        self.Ui = Ui

    def get(self, i, j=None):
        if j is None:
            j = i
        row = self.LUi[i, :]
        col = self.L.T[:, j]

        return (row * col)[0, 0]
