import numpy as np
from math import sqrt
from itertools import izip

class SigmaPoints(object):
    """
    Base class for a set of sigma points. Has as attributes
    points and weights, the former being a list of column vectors
    of same length as weights, which should be a list of floats.
    """

    def __init__(self, points, weights):
        self.points = points
        self.weights = weights

    def weighted_mean(self):
        ret = 0
        for p, w in izip(self.points, self.weights):
            ret += w * p
        return ret

    def weighted_covar(self):
        mean = self.weighted_mean()
        ret = 0
        for p, w in izip(self.points, self.weights):
            ret += w * (p - mean) * (p - mean).T
        return ret

    def propagate(self, function):
        return SigmaPoints([function(p, i) for i,p in enumerate(self.points)], self.weights)


class SimplexSigmaPoints(SigmaPoints):
    """
    Note that the first two points/columns are swapped
    relative to the definition of Moireau and Chapelle. As all weights
    are equal, this should not matter, and for the connoisseur it offers
    bonus symmetry.
    """
    def __init__(self, k):
        alpha = 1 / float(k + 1)

        def sigma_entry(d):
            return 1 / sqrt(alpha * d * (d + 1))

        spt = [sigma_entry(i) for i in range(1, k + 1)]
        spts = [spt]

        for d in range(k):
            spt = spt[:]
            spt[d] = -spt[d] * (d + 1)
            if d > 0:
                spt[d - 1] = 0
            spts.append(spt)

        spt_matrix = np.matrix(spts).T
        spt_list = [spt_matrix[:, i] for i in range(k + 1)]

        self.points = spt_list
        self.weights = [1 / float(k + 1) for i in range(k + 1)]
