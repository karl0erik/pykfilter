pykfilter
=========

Description
-----------
Python implementation of a simplex reduced unscented Kalman filter.

Author
------
Karl Erik Holter karl0erik@gmail.com

Contributors
------------
Gabriel Balaban gabrib@simula.no

Bitbucket site
---------------
`https://bitbucket.org/karl0erik/pykfilter <https://bitbucket.org/karl0erik/pykfilter>`_

Dependencies
------------
NumPy
    `NumPy homepage. <http://www.numpy.org/>`_



Installation
----------

Either install to default python location as root

    sudo python setup.py install

Or install to your own python path directory::

    python setup.py install --prefix=/path/to/my/own/site-packages




Licensing
---------

pykfilter is Copyright 2014 by Karl Erik Holter and licensed under GNU GPL 3.
