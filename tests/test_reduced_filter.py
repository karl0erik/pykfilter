from pykfilter import ReducedSimplexUnscentedKalmanFilter as RSUKF
from pykfilter.sigma_points import SigmaPoints, SimplexSigmaPoints
import numpy as np
from itertools import izip

EPS = 1e-4
MINI_EPS = 1.0e-16

def _ensure_2D(arr):
    """Converts 1D numpy arrays into 2D."""

    if len(arr.shape) == 1:
        return np.matrix([arr])
    else:
        return arr


def _gauss_noise(covar):
    s = covar.shape[0]
    noise = np.random.multivariate_normal(np.zeros(s), covar)
    return np.matrix([noise]).T


def _gen_states(trans, obs, noise, init_state, N, noiseless=False):
    """
    Evolves a system over time and observes it.
    """
    obslist = []
    statelist = [init_state]
    for i in range(N):
        state = trans(statelist[-1], i, 0)
        state_obs = obs(state, i, 0)
        if not noiseless:

            state_obs += _gauss_noise(noise)
        obslist.append(state_obs)
        statelist.append(state)

    return statelist, obslist


def _linear_system(F, H):
    trans = lambda s, n, i: _ensure_2D(np.dot(F, s))
    obs = lambda s, n, i: _ensure_2D(np.dot(H, s))

    return trans, obs


def _example_linear_system():
    F = np.matrix([[2, 1], [1, 0.5]])
    H = np.matrix([[1, 1]])

    return _linear_system(F, H)


def _relative_errors(states, estimates):
    return [abs(est - state) / np.linalg.norm(state)
            for est, state in izip(estimates, states)]


def test_on_deterministic_linear_system():
    # So much noise!
    noise_covar = np.eye(1)*10

    trans, obs = _example_linear_system()
    x0 = np.matrix([[1], [0]])
    U_init = np.matrix([[1, 0], [0, 2]])
    L_init = np.eye(2)

    statelist, obslist = _gen_states(trans, obs, noise_covar, x0, 20, True)

    rukf = RSUKF(trans, obs, noise_covar, 2)

    result = rukf.filter(x0, L_init, U_init, obslist)
    estlist, predlist = result.state_estimates, result.state_predictions

    # though one would expect the estimates and states to be _equal_,
    # this is sadly not the case because of machine error, but
    # just look at that tolerance!

    assert np.allclose(estlist, statelist, atol=MINI_EPS)


def test_on_nondeterministic_linear_system():
    # So much noise!
    noise_covar = np.eye(1)*10

    trans, obs = _example_linear_system()
    x0 = np.matrix([[1], [0]])
    U_init = np.matrix([[1, 0], [0, 2]])
    L_init = np.eye(2)
    statelist, obslist = _gen_states(trans, obs, noise_covar, x0, 20, False)

    rukf = RSUKF(trans, obs, noise_covar, 2)

    result = rukf.filter(x0, L_init, U_init, obslist)
    
    estlist, predlist = result.state_estimates, result.state_predictions 
    # eventually the filter should converge

    assert (_relative_errors(statelist, estlist)[-1] < EPS).all()


def test_on_rank_1_3D_linear_system():
    """
    Now L is 1x3 and U is 1x1, meaning that the covariance matrix
    is 3x3 of rank 1. So in a sense we are reducing a 3D problem
    to a 1D one.
    """

    F = np.matrix([[3.0, 1, 4], [1, 0, 1], [2, 5, 6]])
    H = np.matrix([1, 1, 1])
    trans, obs = _linear_system(F, H)

    L_init = np.matrix([[1], [1], [1]])
    U_init = np.eye(1)
    x0 = np.matrix([[1], [0], [0]])
    noise_covar = np.eye(1) * 100

    statelist, obslist = _gen_states(trans, obs, noise_covar, x0, 10, False)

    rukf = RSUKF(trans, obs, noise_covar, 1)
    result = rukf.filter(x0, L_init, U_init, obslist)
    estlist, predlist = result.state_estimates, result.state_predictions
    assert (_relative_errors(statelist, estlist)[-1] < EPS).all()


def _mandelbrot_system():
    def trans(state, n, i):
        x = state[0, 0]
        y = state[1, 0]
        cr = state[2, 0]
        ci = state[3, 0]

        x_n = x**2 - y**2 + cr
        y_n = 2*x*y + ci
        return np.matrix([[x_n], [y_n], [cr], [ci]])

    obs = lambda s, n, i: np.dot(np.matrix([[1, 0, 0, 0], [0, 1, 0, 0]]), s)
    return trans, obs




def _true_values():
    """
    Hand-calculated (ish) values of all things involved in a reduced
    filtering step of the Mandelbrot system.
    """

    observation = np.matrix([[5],
                             [7]])

    trans, obs = _mandelbrot_system()
    V = np.matrix([[1, 0, -1],
                   [0, 1, -1]])

    # As this has been tested, I don't feel too guilty about using it.
    I = SimplexSigmaPoints(2)

    D = np.matrix(np.diag(I.weights))
    PaV = V * D * V.T

    x0 = np.matrix([[3],
                    [1],
                    [4],
                    [1]])
    L0 = np.matrix([[1, 0],
                    [1, 0],
                    [0, 1],
                    [0, 1]])
    U0 = np.matrix(np.eye(2))
    C0 = np.matrix(np.eye(2))
    W  = np.matrix(np.eye(2))
    Wi = W.I

    x0_sigmas = I.propagate(lambda pt, i: x0 + L0 * C0 * pt)
    x1_sigmas = x0_sigmas.propagate(lambda pt, i: trans(pt, 0, i))
    x1_pred = x1_sigmas.weighted_mean()
    L1 = np.matrix(np.hstack(x1_sigmas.points)) * D * V.T

    obs_sigmas = x1_sigmas.propagate(lambda pt, i: np.matrix([[1, 0, 0, 0],
                                                           [0, 1, 0, 0]]) * pt)
    Z = np.hstack(obs_sigmas.points)
    HL = Z * D * V.T
    U1 = PaV + HL.T * Wi * HL
    obs_err = observation - obs_sigmas.weighted_mean()

    x1_update = x1_pred + L1 * U1.I * HL.T * Wi * obs_err
    relerr = np.linalg.norm(obs_err) / np.linalg.norm(observation)

    rukf = RSUKF(trans, obs, Wi, 2)

    return (x0, x0_sigmas, U0, L0, x1_sigmas, x1_pred,
            x1_update, L1, U1.I, relerr, observation, rukf)

def test_sample():
    x0, x0s, U0, L0, _, _,  _, _, _, _, _, rukf = _true_values()
    x0s_f = rukf.sample(x0, U0, L0)

    assert np.allclose(x0s.points, x0s_f.points)
    assert np.allclose(x0s.weights, x0s_f.weights)

def test_predict():
    _, x0s, _, _, x1s, x1p, _, _, _, _, _, rukf = _true_values()
    x1s_f = rukf.predict(0, x0s)

    assert np.allclose(x1s.points, x1s_f.points)
    assert np.allclose(x1s.weights, x1s_f.weights)
    assert np.allclose(x1p, x1s_f.weighted_mean())

def test_update():
    _, _, __, _, x1s, _, x1u, L1, U1i, relerr, obs, rukf = _true_values()
    x1u_f, L1_f, U1i_f, relerr_f = rukf.correct(1, x1s, obs, True)

    assert np.allclose(x1u, x1u_f)
    assert np.allclose(L1, L1_f)
    assert np.allclose(U1i, U1i_f)
    assert np.allclose(relerr, relerr_f)
